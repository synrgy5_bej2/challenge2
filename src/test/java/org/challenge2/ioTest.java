package org.challenge2;

import org.junit.jupiter.api.*;

import java.util.List;

public class ioTest {


    @Test
    @DisplayName("READ FILE SUCCESS - POSITIVE CASE")
    void readFileSuccess() {
        IOStream ios = new IOStream();

        String path = "D:\\BEJ\\Challenge3\\challenge2\\src\\main\\resources\\data_sekolah.csv";
        List<Integer> resultList = Assertions.assertDoesNotThrow(()->ios.readCSV(path));
        Assertions.assertEquals(198, resultList.size());
    }

    @Test
    @DisplayName("READ FILE GAGAL - NEGATIVE CASE")
    void readFile_NotFound() {
        IOStream ios = new IOStream();
        String path = "D:\\BEJ\\Challenge3\\challenge2\\src\\main\\data_sekolah.csv";

        List<Integer> resultList2 = Assertions.assertDoesNotThrow(()->ios.readCSV(path));
        Assertions.assertEquals(127, resultList2.get(resultList2.size()-1));
        /*Pada Code ioStream buatan saya, apabila File Not Found,
        maka mengembalikan List of Integer dengan elemen terakhirnya sebesar 127
         */
    }

}
