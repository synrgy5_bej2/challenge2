package org.challenge2;

import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.List;

public class statsTest {

    List<Integer> arrayNilai = Arrays.asList(10,20,30,40,50,60,70,80,90,100,100);

    int[][] intArray2D = {  {5,6,7,8,9,10},  {1,2,62,42,50,41}   };

    double hasilMean = 0;
    double hasilMedian = 0;
    int hasilModus = 0;

    @AfterEach
    void afterEach() {
        hasilMean = 0;
        hasilMedian = 0;
        hasilModus = 0;
    }

    @Test
    @DisplayName("Test Hitung Mean - POSITIVE CASE")
    void calculateMeanPOS() {
        Stats stats = new Stats();

        hasilMean = stats.calculateMean(arrayNilai);
        Assertions.assertEquals(59.09090909090909, hasilMean);
    }

   @Test
   @DisplayName("Test Hitung Mean - NEGATIVE CASE")
   void calculateMeanNEG() {
       Stats stats = new Stats();

       hasilMean = stats.calculateMean(null);
       Assertions.assertEquals(0, hasilMean);
//        Assertions.assertThrows(NullPointerException.class, () -> stats.calculateMean(null));
   }

   @Test
   @DisplayName("Test Hitung Median - POSITIVE CASE")
   void calculateMedianPOS() {
       Stats stats = new Stats();

       hasilMedian = stats.calculateMedian(arrayNilai);
       Assertions.assertEquals(60.0, hasilMedian);
   }

   @Test
   @DisplayName("Test Hitung Median - NEGATIVE CASE")
   void calculateMedianNEG() {
       Stats stats = new Stats();

       hasilMedian = stats.calculateMedian(null);
       Assertions.assertEquals(0, hasilMedian);
   }

   @Test
   @DisplayName("Test Hitung Modus - POSITIVE CASE")
   void calculateModusPOS() {
       Stats stats = new Stats();

       hasilModus = stats.calculateModus(intArray2D);
       Assertions.assertEquals(7, hasilModus);
   }

   @Test
   @DisplayName("Test Hitung Modus - NEGATIVE CASE")
   void calculateModusNEG() {
       Stats stats = new Stats();

       hasilModus = stats.calculateModus(null);
       Assertions.assertEquals(0, hasilModus);
   }

}
