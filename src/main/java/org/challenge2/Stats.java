package org.challenge2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.IntStream;


public class Stats {

    /**
     * Fungsi mengembalikan nilai rata-rata seluruh data di file csv.
     *
     * @param   listData   list of Integer yang elemennya terdiri dari nilai yang sudah di-sort ascending
     * @return  mean         nilai average dalam double
     */
    public double calculateMean(List<Integer> listData){
        try {
            return listData.stream().mapToInt(Integer::intValue).average().getAsDouble();
        }
        catch(NullPointerException | NumberFormatException i) {
            System.out.println("NULL");
        }
        return 0.0;

    }

    /**
     * Fungsi mengembalikan nilai median seluruh data di file csv.
     *
     * @param   listData   list of Integer yang elemennya terdiri dari nilai yang sudah di-sort ascending
     * @return  median       nilai tengah dalam double
     */
    public double calculateMedian(List<Integer> listData){

        try {
            return listData.stream().sorted().skip(Math.max(0, ((listData.size() + 1) / 2) - 1))
                    .limit(1 + (1 + listData.size()) % 2).mapToInt(Integer::intValue).average().getAsDouble();

        }
        catch (NullPointerException | NumberFormatException i) {
            System.out.println("NULL");
        }
        return 0.0;

    }

    /**
     * Fungsi mengembalikan frekuensi kemunculan suatu angka pada data di csv
     *
     * @param   arrayNilai  list of Integer yang elemennya terdiri dari nilai  yang sudah di-sort ascending
     * @param   num         angka yang ingin dihitung frekuensi kemunculannya
     * @return  frekuensi   frekuensi kemunculan angka num
     */
    public int countFrequency(List<Integer> arrayNilai, int num) {
        return (int) arrayNilai.stream().filter(value-> num == value).count();
    }

    /**
     * Fungsi mengembalikan tabel berisi nilai dan frekuensi kemunculannya
     *
     * @param   arrayNilai  list of Integer yang elemennya terdiri dari nilai yang sudah di-sort ascending
     * @param   elemenUnik  jumlah elemen nilai yang unik dari seluruh data di csv dalam long
     * @return  tabel       tabel frekuensi (nilai, frekuensi) berupa array integer 2-dimensi
     */
    public int[][] createFreqTable(List<Integer> arrayNilai, long elemenUnik) {
        int[][] tabel = new int[2][(int) elemenUnik];
        HashSet<Integer> set = new HashSet<>();
        long count = 0;
        for (int b : arrayNilai) {
            if (!set.contains(b)) {
                set.add(b);
                tabel[0][(int) (count)] = b;
                tabel[1][(int) (count)] = countFrequency(arrayNilai, b);
                count++;
            }
            if (count == elemenUnik) {
                break;
            }
        }
        return tabel;
    }

    /**
     * Fungsi mengembalikan nilai modus dari seluruh data
     *
     * @param   tabelFrekuensi  tabel frekuensi (nilai, frekuensi) berupa array integer 2-dimensi
     * @return  modus           nilai yang paling banyak muncul
     */
    public int calculateModus(int[][] tabelFrekuensi) {

        try {
            int[] arrayAwal = tabelFrekuensi[1];
            int maxVal = Arrays.stream(arrayAwal).max().getAsInt();
            int indeks = IntStream.range(0, arrayAwal.length).filter(i -> maxVal == tabelFrekuensi[1][i])
                    .findAny().getAsInt();
            return tabelFrekuensi[0][indeks];
        }
        catch (NullPointerException | NumberFormatException i) {
            System.out.println("NULL");
        }
        return 0;

    }

}
