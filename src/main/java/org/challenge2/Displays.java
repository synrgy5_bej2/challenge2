package org.challenge2;

import java.util.Scanner;

public class Displays {

    static String headerMenu = "\n----------------------------- \nAplikasi Pengolah Nilai Siswa \n----------------------------- \n";

    /**
     * Mencetak menu utama pada layar.
     *
     */
    public static void printMainMenu(){
        System.out.println(headerMenu +
                "Letakkan file csv dengan nama file data_sekolah di direktori berikut : D:\\BEJ\\Challenge3\\challenge2\\src\\main\\resources \n" +
                "\n Menu: \n" +
                "1. Generate txt untuk mengelompokkan data berdasarkan frekuensi\n" +
                "2. Generate txt untuk menyajikan mean, median, modus \n" +
                "3. Generate kedua file \n" +
                "0. Exit \n");
    }

    /**
     * Mencetak pesan sukses (generating txt) pada layar.
     *
     */
    public static void printSuccessMessage(){
        System.out.println(headerMenu +
                "File txt telah di generate di direktori berikut : D:\\BEJ\\Challenge3\\challenge2\\src\\main\\resources \n" +
                "Silakan cek \n" +
                "\n0. Exit \n" +
                "1. Kembali ke menu utama \n");
    }

    /**
     * Mencetak pesan error (file tidak ditemukan pada direktori) pada layar.
     *
     */
    public static void printErrorMessage(){
        System.out.println(headerMenu +
                "File csv tidak ditemukan \n" +
                "\n0. Exit \n" +
                "1. Kembali ke menu utama \n");
    }

    /**
     * Mencetak sub-menu (inner loop) pada layar
     * dan Mengembalikan pilihan menu dalam byte.
     *
     * @return pilihan  menu yang dipilih pengguna dalam byte
     */
    public static byte subMenu(){
        byte pilihan = 2;
        Scanner scan = new Scanner(System.in);
        while (true) {
            printSuccessMessage();
            try {
                pilihan = scan.nextByte();
                if (pilihan < 0 || pilihan > 1) {
                    throw new IllegalArgumentException("Input tidak valid");
                } else {
                    return pilihan;
                }
            }
            catch(IllegalArgumentException i) {
                System.out.println("Input tidak valid (out of range). Silakan input ulang.");
            }
        }
    }

    /**
     * Mencetak main menu pada layar
     * dan Mengembalikan pilihan menu dalam byte.
     *
     * @return pilihan  menu yang dipilih pengguna dalam byte
     */
    public static byte readInputMenu() {
        byte pilihan = 2;
        Scanner scan = new Scanner(System.in);
        while (true) {
            printMainMenu();
            try {
                pilihan = scan.nextByte();
                if (pilihan < 0 || pilihan > 3) {
                    throw new IllegalArgumentException("Input tidak valid");
                } else {
                    return pilihan;
                }
            }
            catch(IllegalArgumentException i) {
                System.out.println("Input tidak valid (out of range). Silakan input ulang.");
            }
        }
    }

}
