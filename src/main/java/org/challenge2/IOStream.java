package org.challenge2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IOStream {

    Stats stats = new Stats();

    /**
     * Fungsi membaca file csv dan mengembalikan list of Integer dari seluruh data nilai di csv
     *
     * @param   namafile    direktori path file
     * @return  arrayData   list of Integer seluruh data nilai
     */
    public List<Integer> readCSV(String namafile)    {
        List<Integer> arrayData = new ArrayList<>();//new ArrayList<>(Arrays.asList())
        try {
            Scanner sc = new Scanner(new FileReader(namafile));
            String pattern = "[a-zA-Z]*";
            while (sc.hasNextLine()){
                String lineBaru = sc.nextLine();
                try( Scanner token = new Scanner(lineBaru).skip(pattern).useDelimiter(";")) {
                    while (token.hasNext()){
                        int angka = Integer.parseInt(token.next());
                        arrayData.add(angka);
                    }
                }
            }
            sc.close();
        }
        catch (FileNotFoundException e) {
            arrayData.add(127);
            wait(5000);
        }
        return arrayData;
    }

    /**
     * Menulis tabel frekuensi nilai pada file .txt
     *
     * @param   elemenUnik      jumlah elemen nilai yang unik dari seluruh data di csv dalam long
     * @param   tabelFrekuensi  tabel frekuensi (nilai, frekuensi) berupa array integer 2-dimensi
     */
    public void writeTXT1(long elemenUnik, int[][] tabelFrekuensi){
        try{
            int i;
            PrintWriter pw = new PrintWriter(new FileWriter("D:\\BEJ\\Challenge3\\challenge2\\src\\main\\resources\\data_sekolah1.txt"));
            pw.println("Berikut Hasil Pengolahan Nilai: \n");
            pw.println("Nilai\t|\tFrekuensi");
            for (i = 0; i < (int)(elemenUnik); i++){
                pw.printf("%d\t\t|\t\t%d%n", tabelFrekuensi[0][i], tabelFrekuensi[1][i]);
            }
            pw.close();
        }
        catch (IOException e) {
            System.out.println("ERROR: Could not write");
        }
    }

    /**
     * Menulis mean, median, dan modus pada file .txt
     *
     * @param   arrayNilai  list of Integer yang elemennya terdiri dari nilai yang sudah di-sort ascending
     * @param   tabelFrekuensi  tabel frekuensi (nilai, frekuensi) berupa array integer 2-dimensi
     */
    public void writeTXT2(List<Integer> arrayNilai, int[][] tabelFrekuensi){
        try{
            PrintWriter pw = new PrintWriter(new FileWriter("D:\\BEJ\\Challenge3\\challenge2\\src\\main\\resources\\data_sekolah2.txt"));
            pw.println("Berikut Hasil Pengolahan Nilai: \n");
            pw.println("Berikut hasil sebaran data nilai");
            pw.printf("Mean : %.2f%n", stats.calculateMean(arrayNilai));
            pw.printf("Median : %.2f%n", stats.calculateMedian(arrayNilai));
            pw.printf("Modus : %d", stats.calculateModus(tabelFrekuensi));
            pw.close();
        } catch (IOException e) {
            System.out.println("ERROR: Could not write");
        }
    }

    /**
     * Menghasilkan .file txt berdasarkan input pengguna
     *
     * @param   pilihan         menu yang dipilih pengguna dalam byte
     * @param   dataArray       list of Integer yang elemennya terdiri dari nilai yang sudah di-sort ascending
     * @param   elemenUnik      jumlah elemen nilai yang unik dari seluruh data di csv dalam long
     * @param   tabelFrekuensi  tabel frekuensi (nilai, frekuensi) berupa array integer 2-dimensi
     */
    public void generateTXT(byte pilihan, List<Integer> dataArray, long elemenUnik, int[][] tabelFrekuensi){
        if (pilihan==1 || pilihan==3 ){
            writeTXT1(elemenUnik,tabelFrekuensi);
        }
        if (pilihan==2 || pilihan==3){
            writeTXT2(dataArray,tabelFrekuensi);
        }
    }

    /**
     * Menghasilkan delay selama 5 detik
     *
     * @param   ms      lama delay dalam milisekon
     */
    public void wait(int ms)    {
        try        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)        {
            Thread.currentThread().interrupt();
        }
    }

}
