import java.util.*;
import org.challenge2.*;

import static org.challenge2.Displays.*;

public class Main {

    public static void main(String[] args) {
        IOStream ios = new IOStream();
        Stats stats = new Stats();

        String namaFile = "D:\\BEJ\\Challenge3\\challenge2\\src\\main\\resources\\data_sekolah.csv";
        Scanner input = new Scanner(System.in);
        byte pilihan ;

        pilihan = readInputMenu();
        List<Integer> dataList = ios.readCSV(namaFile);

        while (dataList.get(dataList.size()-1) == 127){
            printErrorMessage();
            pilihan = input.nextByte();
            if (pilihan==0){
                break;
            } else if (pilihan==1) {
                dataList.remove(dataList.size()-1);
                printMainMenu();
                dataList = ios.readCSV(namaFile);
            }
        }

        long elemenUnik = dataList.stream().distinct().count();
        Collections.sort(dataList);
        int[][] tabelFrekuensi;
        tabelFrekuensi = stats.createFreqTable(dataList,elemenUnik);

        while (pilihan != 0){
            ios.generateTXT(pilihan,dataList,elemenUnik,tabelFrekuensi);
            pilihan = subMenu();

            if (pilihan == 1){
                pilihan = readInputMenu();
            }
        }
    }

}
